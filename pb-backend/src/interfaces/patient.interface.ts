/* eslint-disable prettier/prettier */
export interface Patient {
  id: number;
  room?: string;
  hn: string;
  firstName: string;
  lastName: string;
}
