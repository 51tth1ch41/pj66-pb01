/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Patient } from './interfaces/patient.interface';

@Injectable()
export class PatientsService {
  async findAll(): Promise<Patient[]> {
    return [
      {
        id: 1,
        room: '1214C',
        hn: '0517246',
        firstName: 'สิทธิชัย',
        lastName: 'หลวงแสง',
      },
    ]
  }
}
