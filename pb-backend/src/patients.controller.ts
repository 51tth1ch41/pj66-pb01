/* eslint-disable prettier/prettier */
import { Controller, Get } from '@nestjs/common';
import { Patient } from './interfaces/patient.interface';
import { PatientsService } from './patients.service';

@Controller('patients')
export class PatientsController {
  constructor(private patientSerivce: PatientsService) {}

  @Get()
  async findAll(): Promise<Patient[]> {
    return this.patientSerivce.findAll();
  }
}
