import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PatientsController } from './patients.controller';
import { PatientsService } from './patients.service';

@Module({
  imports: [],
  controllers: [AppController, PatientsController],
  providers: [AppService, PatientsService],
})
export class AppModule {}
